import sys
import zipfile

def GetContent(xmindfilename):
    zip = zipfile.ZipFile(xmindfilename);
    contentContent = zip.read("content.xml")
    #contentContent = contentContent.replace("<", "\n<")
    contentContent = contentContent.replace(">", ">\n")
    return contentContent
    
    
if __name__ == "__main__":
    if len(sys.argv) > 1:
        displayableContent = GetContent(sys.argv[1])
        print(displayableContent)
